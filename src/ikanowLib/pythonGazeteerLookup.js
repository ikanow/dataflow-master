( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var pythonGazeteerLookup = Dataflow.prototype.node("Python Gazeteer Lookup");

  pythonGazeteerLookup.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "Python Gazeteer Lookup";
      defaults.label = "Python Gazeteer Lookup";
      defaults.w = 205;
      defaults.h = 100;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "List of Files",
		type: "string"
		}
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  pythonGazeteerLookup.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var splitOnRegexMatch = Dataflow.prototype.node("3. Splitting / Split on Regex Match");

  splitOnRegexMatch.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "3. Splitting / Split on Regex Match";
      defaults.label = "Split on Regex Match";
      defaults.w = 200;
      defaults.h = 105;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "Fieldname to test",
		type: "string"
		},{
		id: "Regex",
		type: "string"
		}//,
	  //{
	//	id : "delete_when_complete",
	//	type:"boolean",
	//	value:false
	 // }
      // },
      // {
      //   id: "select2",
      //   type: "int",
      //   min: 0,
      //   max: 3,
      //   options: {sine:0, square:1, saw: 2, triangle: 3}
      // }
    ],
    outputs:[
      {
        id: "True",
        type: "float"
      },
	  {
        id: "False",
        type: "float"
      }
    ]
  });

  splitOnRegexMatch.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var netOwlEntityAssociationExtractor = Dataflow.prototype.node("1. Inputs / NetOwl Entity/Assocation Extractor");

  netOwlEntityAssociationExtractor.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "1. Inputs / NetOwl Entity/Assocation Extractor";
      defaults.label = "NetOwl Entity/Assocation Extractor";
      defaults.w = 205;
      defaults.h = 100;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		}
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  netOwlEntityAssociationExtractor.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
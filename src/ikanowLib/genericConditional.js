( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var genericConditional = Dataflow.prototype.node("3. Splitting / Generic Conditional");

  genericConditional.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "3. Splitting / Generic Conditional";
      defaults.label = "Generic Conditional";
      defaults.w = 205;
      defaults.h = 100;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "Split Criterion",
		type: "string"
		}//,
	  //{
	//	id : "delete_when_complete",
	//	type:"boolean",
	//	value:false
	 // }
      // },
      // {
      //   id: "select2",
      //   type: "int",
      //   min: 0,
      //   max: 3,
      //   options: {sine:0, square:1, saw: 2, triangle: 3}
      // }
    ],
    outputs:[
      {
        id: "True",
        type: "float"
      },
	  {
        id: "False",
        type: "float"
      }
    ]
  });

  genericConditional.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var jsonFileExtractor = Dataflow.prototype.node("1. Inputs / JSON Local File Extractor");

  jsonFileExtractor.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "1. Inputs / JSON Local File Extractor";
      defaults.label = "JSON Local File Extractor";
      defaults.w = 205;
      defaults.h = 205;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Path",
		type: "string"
		},{
		id: "Delete Once Done",
		type: "boolean"
		},{
        id: "Include Filter",
        type: "string"
      },{
        id: "Exclude Filter",
        type: "string"
      },
      {
        id: "Root Objects",
        type: "string"
      },
      {
        id: "Ignore Objects",
        type: "string"
      },
      {
        id: "Primary Key",
        type: "string"
      },
      {
        id: "URL Prefix",
        type: "string"
      }//,
	  //{
	//	id : "delete_when_complete",
	//	type:"boolean",
	//	value:false
	 // }
      // },
      // {
      //   id: "select2",
      //   type: "int",
      //   min: 0,
      //   max: 3,
      //   options: {sine:0, square:1, saw: 2, triangle: 3}
      // }
    ],
    outputs:[
      {
        id: "output",
        type: "float"
      }
    ]
  });

  jsonFileExtractor.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      // this.$inner.text("Ingests various file types from their locations and performs processing based on the configuration.");
    }
  });

}(Dataflow) );
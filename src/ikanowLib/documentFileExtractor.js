( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var documentFileExtractor = Dataflow.prototype.node("1. Inputs / Document Local File Extractor");

  documentFileExtractor.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "1. Inputs / Document Local File Extractor";
      defaults.label = "Document Local File Extractor";
      defaults.w = 200;
      defaults.h = 125;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Path",
		type: "string"
		},{
		id: "Delete Once Done",
		type: "boolean"
		},{
        id: "Include Filter",
        type: "string"
      },{
        id: "Exclude Filter",
        type: "string"
      }//,
	  //{
	//	id : "delete_when_complete",
	//	type:"boolean",
	//	value:false
	 // }
      // },
      // {
      //   id: "select2",
      //   type: "int",
      //   min: 0,
      //   max: 3,
      //   options: {sine:0, square:1, saw: 2, triangle: 3}
      // }
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  documentFileExtractor.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      // this.$inner.text("Ingests various file types from their locations and performs processing based on the configuration.");
    }
  });

}(Dataflow) );
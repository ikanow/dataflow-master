( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var regexToEntity = Dataflow.prototype.node("6. Entities / Regex to Entity");

  regexToEntity.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "6. Entities / Regex to Entity";
      defaults.label = "Regex to Entity";
      defaults.w = 205;
      defaults.h = 150;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "Entity Type",
		type: "string"
		},{
		id: "Dimension",
		type: "string"
		},{
		id: "Regex",
		type: "string"
		},{
		id: "Replace",
		type: "string"
		}
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  regexToEntity.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
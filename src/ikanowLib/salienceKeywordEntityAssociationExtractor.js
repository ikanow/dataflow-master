( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var salienceKeywordEntityAssociationExtractor = Dataflow.prototype.node("1. Inputs / Salience Keyword/Entity/Association Extractor");

  salienceKeywordEntityAssociationExtractor.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "1. Inputs / Salience Keyword/Entity/Association Extractor";
      defaults.label = "Salience Keyword/Entity/Association Extractor";
      defaults.w = 205;
      defaults.h = 130;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "Extract Topics",
		type: "string"
		},{
		id: "Extract Subtopics",
		type: "string"
		},{
		id: "Max Keywords To Extract",
		type: "string"
		}
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  salienceKeywordEntityAssociationExtractor.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
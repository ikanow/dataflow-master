( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var salienceEntityAssociationExtractor = Dataflow.prototype.node("1. Inputs / Salience Entity/Association Extractor");

  salienceEntityAssociationExtractor.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "1. Inputs / Salience Entity/Association Extractor";
      defaults.label = "Salience Entity/Association Extractor";
      defaults.w = 205;
      defaults.h = 100;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "Extract Topics",
		type: "string"
		},{
		id: "Extract Subtopics",
		type: "string"
		}
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  salienceEntityAssociationExtractor.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var regexToMetadata = Dataflow.prototype.node("5. Metadata / Regex to Metadata");

  regexToMetadata.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "5. Metadata / Regex to Metadata";
      defaults.label = "Regex to Metadata";
      defaults.w = 205;
      defaults.h = 125;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
     inputs:[
	 {
		id: "Input",
		type: "float"
		},{
		id: "Metadata Field Name",
		type: "int"
		},{
		id: "Regex",
		type: "string"
		},{
		id: "Replace",
		type: "string"
		}
    ],
    outputs:[
      {
        id: "Output",
        type: "float"
      }
    ]
  });

  regexToMetadata.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text(" ");
    }
  });

}(Dataflow) );
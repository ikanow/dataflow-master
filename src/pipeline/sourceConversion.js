//method returns array of errors or empty array if no error
//Node objects contained in  graph.nodes are sorted, validated and then source convert function is applied
function sourceConvert(source,sourceBuilderMap,sourceValidatorMap,graph,dataflow){
	var errors = [];
	var ids = [];
	var nodes = [];
	var nodesByIdMap = {};
	try{
		
	// clone graph so we can expand it.
	var clonedGraph = JSON.parse(JSON.stringify(graph, null, "   "));
	 //ids = orderComponentIds(pipeline);	
	 var originalNodes = clonedGraph.nodes;	 
	 var originalEdges = clonedGraph.edges; 
     var expandedGraphModel = expandSubGraphs(originalNodes,originalEdges);
     nodesByIdMap = expandedGraphModel.nodesById;
	 var plEdges = expandedGraphModel.edges;
	 	 
	 ids = orderGraphNodeIds(plEdges);
	  for (var i = 0; i < ids.length; i++) {	  
		  var n = nodesByIdMap[ids[i]];
		  nodes.push(n);	  
	  }
	}
	catch(e){
	 // If the user creates cyclical graph then the error will show up here
	 console.log(e);
	 //return e.message;
	 errors.push(e.message);
	 //The following loops infinity and cannot reliably catch the errors
	 //if(confirm("The last action made threw an error. Click OK to revert your graph. Cancelling may result in an unstable flow & loss of work.")){
	 //	dataflow.undo();
	 //	}
	}
	

	try{
		var cvErrors = componentBuildAndValidate(source,nodes,plEdges,nodesByIdMap,sourceBuilderMap,sourceValidatorMap);
		// The cvErrors contain a list of errors per component, potentially created by sourceBuilder and sourceValidator functions.
		if(cvErrors!=null && cvErrors.length>0){
		 errors = errors.concat(cvErrors);
		}
	
	// global validate
	var gvErrors = globalValidate(source,nodes,plEdges,nodesByIdMap,sourceBuilderMap,sourceValidatorMap);
	// The global validation errors are created when the graph components are out or order or usingelements that don't make sense given the input type
	
	dataflow.graphErrors = ""
	if(gvErrors!=null && gvErrors.length>0){
	 errors = errors.concat(gvErrors);
	}
	if(errors!=null && errors.length>0){
		var errorMessages ="";
		for (var i = 0; i < errors.length; i++) {
			errorMessages+=errors[i];
			errorMessages+="\n";
			//console.log(errors[i]);
		}
		//console.log("Components contain the errors:\n"+errorMessages);
		dataflow.graphErrors = errorMessages;
	}
	}
	catch(e){
		 console.log(e);		 
		}
	return errors;
}

function expandSubGraphs(nodes,edges){
	var factor = 10000;
	var allNodesByIdMap = {};
	var allEdges = [];
	var subgraphType = "dataflow-subgraph";
	var subgraphInputType = "dataflow-input";
	var subgraphOutputType = "dataflow-output";
	
		// iterate over nodes and expand subgraph components
		for ( var i= 0; i < nodes.length; i++ ){
			var node = nodes[i];
			var id = node.id;
			var sourceLineType = node.type;			
			
			if(sourceLineType==subgraphType){
				var subGraph = node.graph;
				// loop over subnodes and renumber ids
				var subNodes = subGraph.nodes;
				var subInputNodeId = null;
				var subOutputNodeId = null;
				for ( var k= 0; k < subNodes.length; k++ ){
				    var subNode = subNodes[k];
					var newSubNodeId = factor*id+subNode.id;
					subNode.id = newSubNodeId;
					// remember input and output node id
					if(subNode.type==subgraphInputType){
						subInputNodeId = newSubNodeId;
					}else
					if(subNode.type==subgraphOutputType){
						subOutputNodeId = newSubNodeId;
					}else if(subNode.type==subgraphType){
						// TODO fix recursion here
						throw new Error("Nested subgraph elements are not supported at this time.");
					}
					else{
						// append sub nodes but not input and output nodes
						allNodesByIdMap[newSubNodeId]=subNode;
					}
				} // for k

				var subEdges = subGraph.edges;
				for ( var k= 0; k < subEdges.length; k++ ){
				    var subEdge = subEdges[k];
					subEdge.source.node = factor*id+subEdge.source.node;
					subEdge.target.node = factor*id+subEdge.target.node;
					
					// special treatment for edge: (s:input,t:firstNode)  
					if(subEdge.source.node == subInputNodeId){
						var targetSubNode = subEdge.target.node;
						// loop over parent edges and replace edge.target with new targetSubnode
						for( var l = 0; l < edges.length; l++ ){
							var parentEdge = edges[l];
							// check if edge is output pointing to subgraph node
							if(parentEdge.target.node==id){
								parentEdge.target.node = targetSubNode;
								break;
							}
						} // for l					
					}else if(subEdge.target.node == subOutputNodeId)
					{
						
						var sourceSubNode = subEdge.source.node;
						// loop over parent edges and find target node of subgraph
						var parentTargetNodeId = null;						
						for( var l = 0; l < edges.length; l++ ){
							var parentEdge = edges[l];
							// check if edge is output pointing to subgraph node
							if(parentEdge.source.node==id){
								parentTargetNodeId = parentEdge.target.node;
								// remove parentEdge because we will insert sub-edges instead
								edges.splice(l,1);								
								break;
							}
						} // for l
						if(parentTargetNodeId!=null){
							subEdge.target.node = parentTargetNodeId;
							allEdges.push(subEdge);
							// deal with additional edges pointing to output
							// ok tricky, since we already know where the parent target node is, we have to replace the rest of the outputs, too.
							for( var l = k+1; l < subEdges.length; l++ ){
								if(subEdges[l].target.node == subOutputNodeId){
									subEdges[l].target.node = parentTargetNodeId;
								} // if
							} // for subEdges l
						} //if
						 					
					}else{
						// all other edges no connected to inpput and output
						allEdges.push(subEdge);
					}
				} // for k

				// replace outputEdge(s) of parent graph with new graph input edge id
				// replace input edge with output edge of subgraph
			} // if subgraphType
			else{
				// all nodes except subgraph node
				allNodesByIdMap[id]=node;
			}
		} // for i	
		allEdges = allEdges.concat(edges);
	return {
		    nodesById: allNodesByIdMap,
	        edges: allEdges
			};
	}

// function topologically sorts node ids.
// input parameters  are graph.edges.models
// returns array of ids sorted in topological orderComponentIds
// throws Error if cycle is detected 
function orderGraphNodeIds(plEdges){
	//console.log('orderGraphNodeIds function called from sourceConversion.js');

	// create structure for topological sorting of ids;
	var edges = [];
	var sortedIds = [];
	for (var i = 0; i < plEdges.length; i++) {
	    var plE = plEdges[i];
	    var e = [plE.source.node,plE.target.node];
	    edges.push(e);	    
	}
	//console.log('edges:'+edges);
    sortedIds = tsort(edges);
	//console.log('sortedIds:'+sortedIds);
	return sortedIds;
}



//function validates all components together based on global rules, similar to passConditionAfterAdd function
function componentBuildAndValidate(source,nodes,edges,nodesByIdMap,sourceBuilderMap,sourceValidatorMap){
	var errors = [];
	
	var lastSourceElement = null;
	// map source to nodeIds
	var sourcesToNodeId = {};
	var nodeIdToOutputEdgesMap = createNodeIdToOutputEdgesMap(edges); 
	var nodeIdToInputEdgesMap = createNodeIdToInputEdgesMap(edges);

	// connection validate
	var cErrors = connectionsValidate(nodeIdToOutputEdgesMap,nodesByIdMap);
	// The connection validation errors are created when the graph components are wired together from one output to multiple inputs.
	if(cErrors!=null && cErrors.length>0){
	 errors = errors.concat(cErrors);
	}

	// create conditional scripts by nodeIds
	var pathScriptByNodeId = createConditionalScripts(source,nodes,edges,nodesByIdMap,nodeIdToOutputEdgesMap,nodeIdToInputEdgesMap);		

	var defaultSourceBuilderFunc = function(flowElement, source, pipeline, lastSourceElement) {
    // same params as above, called before "sourceBuilder" - if returns a non-null string then source building is interrupted and the string is returned to the user
		//console.log('default source builder function'+flowElement+','+source+','+pipeline+','+lastSourceElement);
		 var element = {
				 display: "Default source builder"+flowElement.type,
				 type: flowElement.type
		}
		pipeline.push(element);
		return element;
	};

	var defaultSourceValidatorFunc = function(flowElement, source, pipeline, lastSourceElement) {
		    // same params as above, called before "sourceBuilder" - if returns a non-null string then source building is interrupted and the string is returned to the user
				//console.log('default source validator function'+flowElement+','+source+','+pipeline+','+lastSourceElement);
				return null;
		};
	
	for ( var i= 0; i < nodes.length; i++ )
	{
		var node = nodes[i];
		if(typeof(node)!="undefined"){
			var sourceLineType = node.type;
			var nodeId = node.id;
			//lookup builder and validation function by type and apply it
			var sourceBuilderFunc = sourceBuilderMap[sourceLineType];
			if(sourceBuilderFunc==null){
			 sourceBuilderFunc = defaultSourceBuilderFunc;
			}
			var nodePipelineElements = [];
			// node somehow contains empty state, so hack here: parsing flowElement so we get the state
			var flowElement = JSON.parse(JSON.stringify(node, null, "   "));
			if (!flowElement.state){
				flowElement.state = {};
			}
			
			// set current path
			var pathScript = pathScriptByNodeId[nodeId];
			flowElement.currentPath = pathScript; 
			try{				
				sourceBuilderFunc(flowElement,source,nodePipelineElements,lastSourceElement);
			}
			catch(e){
				console.log(e);
			}
			if(nodePipelineElements.length>0){
				lastSourceElement = nodePipelineElements[nodePipelineElements.length-1];
			}
			
			// if conditional only attach conditional scripts to last element in list
			var pipeStart=0;
			if(isConditionalNode(nodeId,nodeIdToOutputEdgesMap,nodesByIdMap)){
				pipeStart=nodePipelineElements.length-1;
			}
			// loop backwards and assign criteria scripts
			for ( var k= nodePipelineElements.length-1; k >=pipeStart ; k-- )
			{				
				var criteria = "";
				if(typeof(pathScript)!="undefined"){
					criteria = pathScript;
				}
				var pElement = nodePipelineElements[k];
				if(typeof(pElement.criteria)!="undefined"){
					var origCriteria = pElement.criteria;
					if(origCriteria.indexOf("$SCRIPT") > -1){
						criteria = origCriteria+criteria;
					}else{
						criteria = "$SCRIPT("+origCriteria+")"+criteria;
					}
				} // pElement.criteria
				if(criteria!=""){
					pElement.criteria = criteria;
				}
			}
			// attach pipeline elements to processingPipeline
			source.processingPipeline.push.apply(source.processingPipeline, nodePipelineElements);
			var sourceValidatorFunc = sourceValidatorMap[sourceLineType];
			if(sourceValidatorFunc==null){
				 sourceValidatorFunc = defaultSourceValidatorFunc;
			}
			try{
				var verror = sourceValidatorFunc(flowElement,source,nodePipelineElements,lastSourceElement);
				if(verror!=null){
				   errors.push(verror);
				}
			}
			catch(e){
				//console.log(e);
			}
			
		}// typeof undefined
	} // for nodes
	
	return errors;
}

function createConditionalScripts(source,nodes,edges,nodesByIdMap,nodeIdToOutputEdgesMap,nodeIdToInputEdgesMap){
	
	var setPathScriptByNodeId = {};	
	var pathScriptByNodeId = {};	
    
	for ( var i= 0; i < nodes.length; i++ )
	{
		var node = nodes[i];
		var sourceLineType = node.type;
		var nodeId = node.id;
		// handle conditionals
		if(isConditionalNode(nodeId,nodeIdToOutputEdgesMap,nodesByIdMap)){		
			
			var setPathScript = createSetPathScript(node,nodeIdToOutputEdgesMap);
			if(setPathScript!=null){			
			
				setPathScriptByNodeId[nodeId]=setPathScript;
			    	
				var tempPathMap = propagatePathScriptlets(node,i,source,nodes,edges,nodesByIdMap,nodeIdToOutputEdgesMap,nodeIdToInputEdgesMap);
				// merge maps, append to existing path scripts
				
				for (var nodeId in tempPathMap) {
				  if (pathScriptByNodeId.hasOwnProperty(nodeId)) {
					  var existingScript = tempPathMap[nodeId];
					  // overwrite path script with latest
					pathScriptByNodeId[nodeId]=tempPathMap[nodeId];
				  }else{
					pathScriptByNodeId[nodeId]=tempPathMap[nodeId];
				  }
				} // for nodeId
			} // if setPathscript
		} // if		
	} // for

	// concatenate SETPATH scripts
	for (var nodeId in setPathScriptByNodeId) {
		  if (pathScriptByNodeId.hasOwnProperty(nodeId)) {
			  
			pathScriptByNodeId[nodeId]=setPathScriptByNodeId[nodeId]+pathScriptByNodeId[nodeId];
		  }else{
			pathScriptByNodeId[nodeId]=setPathScriptByNodeId[nodeId];
		  }
		} // for nodeId

	return pathScriptByNodeId;
}

function isConditionalNode(nodeId,nodeIdToOutputEdgesMap,nodesByIdMap){	
	var edges = nodeIdToOutputEdgesMap[nodeId];
	// TODO this is not a good solution to parse the type outof  the name. We should pass the type on theflowbuilder component instead. 
/*	if((typeof(edges)!="undefined") && (edges.length>1)){
		return true;
	}else{
	*/
		var splittedNode = nodesByIdMap[nodeId].label.split("/");
		return ("conditional"==splittedNode[1]);
	//}
	return false;
}

function isMergeNode(nodeId,nodeIdToInputEdgesMap){
	var edges = nodeIdToInputEdgesMap[nodeId];
	return (typeof(edges)!="undefined") && edges.length>1;
}

//function validates all components together based on global rules, similar to passConditionAfterAdd function
function propagatePathScriptlets(conditionalNode,currentIndex,source,nodes,edges,nodesByIdMap,nodeIdToOutputEdgesMap,nodeIdToInputEdgesMap){

	var nodeIdToPathScriptMap = {};
	// check if conditional element contains criteria

	var conditionalId = conditionalNode.id;
	// TODOD create testcase where only one output is used in conditional
	//if(conditionalNode.outputs.models.length<1){
		//throw new Error("Conditional node "+conditionalId+" has only one output.");
	//}
	
	// initialize current output nodes with pathScripts
	for ( var i = 0; i < nodeIdToOutputEdgesMap[conditionalId].length; i++ )
	{
		var outputEdge = nodeIdToOutputEdgesMap[conditionalId][i];
		var trueOrFalse= outputEdge.source.port;
		var pathScript = createPathScript(conditionalId,trueOrFalse);
		var nextNodeId = outputEdge.target.node;
		var nextNode = nodesByIdMap[nextNodeId];
		if(!isMergeNode(nextNodeId,nodeIdToInputEdgesMap)){
			// only set path if node is not end node,e.g. does not have multiple inputs
			// assumption is that this node also does not have any outputs
			nodeIdToPathScriptMap[nextNodeId]=pathScript;
		}
	}
	
	// loop over rest of nodes towards the end
	for ( var i= currentIndex+1; i < nodes.length ; i++ )
	{
		var nodeId = nodes[i].id;
		// we need to check if nodes do not have script yet otherwise they have been initialized as the nodes right after the conditional. 
		if(nodeIdToPathScriptMap[nodeId]==null){
			var inputEdgeList = nodeIdToInputEdgesMap[nodeId];
			var outputEdgeList = nodeIdToOutputEdgesMap[nodeId];
			//if((inputEdgeList.length>1) || (typeof(outputEdgeList)=== "undefined") ){
			if(inputEdgeList.length>1){
				// we are done, arrived at entity with two inputs
				//or end here if we are at node without outputs
				break;
				
			}else if(inputEdgeList.length==1){
				// find predecessor script and decide if copy path script  to current or create it from conditional output.
				// the assumption is that predecessor is initialized because nodes are sorted.
				var inputEdge = inputEdgeList[0];			
				var prevNodeId = inputEdge.source.node;
				var pathScript = nodeIdToPathScriptMap[prevNodeId];
				if(typeof(pathScript)!= "undefined"){
					// if undefined then we hit a node that is on a path that cannot be reached from here,
					// otherwise set the script
					nodeIdToPathScriptMap[nodeId]=pathScript;
				}				
			}
		}// if ==null		
	}// for i
	return nodeIdToPathScriptMap;
}

function createSetPathScript(conditionalNode,nodeIdToOutputEdgesMap){
	// initialize current output nodes with pathScripts
	var conditionalId = conditionalNode.id;
	var setPath = null;	
	setPath="$SETPATH("+conditionalId+"_True,"+conditionalId+"_False)";
	return setPath;
}


function createPathScript(conditionalId,trueOrFalse,criteria){
	var pathScript = "$PATH("+conditionalId+"_"+trueOrFalse+")";
	return pathScript;
}

function createNodeIdToOutputEdgesMap(edges){
	var nodeIdToEdgesMap ={};
	for ( var i = 0; i < edges.length; i++ )
	{
		var e = edges[i];
		//Function
		var nodeId = e.source.node;
		var edgeList = nodeIdToEdgesMap[nodeId];
		if(edgeList == null){
			edgeList = [];
			nodeIdToEdgesMap[nodeId]=edgeList;
		}
		edgeList.push(e);
	} // for
	return nodeIdToEdgesMap;
}

function createNodeIdToInputEdgesMap(edges){
	var nodeIdToEdgesMap ={};
	for ( var i = 0; i < edges.length; i++ )
	{
		var e = edges[i];
		//Function
		var nodeId = e.target.node;
		var edgeList = nodeIdToEdgesMap[nodeId];
		if(edgeList == null){
			edgeList = [];
			nodeIdToEdgesMap[nodeId]=edgeList;
		}
		edgeList.push(e);
	} // for
	return nodeIdToEdgesMap;
}

//helper function to extract objects by id or by name
//array = [{key:value},{key:value}]
function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

function connectionsValidate(nodeIdToOutputEdgesMap,nodesByIdMap){
	var errors = [];
	for (var nodeId in nodeIdToOutputEdgesMap) {
		  if (nodeIdToOutputEdgesMap.hasOwnProperty(nodeId)) {
		    var edges = nodeIdToOutputEdgesMap[nodeId];
		    if(edges.length>1){
		    	if(!isConditionalNode(nodeId,nodeIdToOutputEdgesMap,nodesByIdMap)){
		    		errors.push("NodeId:"+nodeId+" has one output connected to multiple components.");
		    	}
		    }
		  }
	}
	return errors;
}

//function validates all components together based on global rules, similar to passConditionAfterAdd function
function globalValidate(source,nodes,plEdges,nodesByIdMap,sourceBuilderMap,sourceValidatorMap){
	
	/* console.log("source");console.dir(source);
	console.log("nodes");console.dir(nodes);
	console.log("plEdges");console.dir(plEdges);
	console.log("nodesByIdMap");console.dir(nodesByIdMap);
	console.log("sourceBuilderMap");console.dir(sourceBuilderMap);
	console.log("sourceValidatorMap");console.dir(sourceValidatorMap); */
	var pipeline = source.processingPipeline;
	var errors = [];
	var isFirstExtractor;
	//var firstFolder= ""; //:String 
	var nExtractors= 0; //:int 
	var nExtractor_Feed = 0; //:int
	var nExtractor_Web = 0; //:int
	
	var nGlobals = 0;//:int
	var nGlobal_Harvest = 0;
	var nGlobal_Javascript = 0; //:int
	var nGlobal_LookupTables = 0; //:int
	var nGlobal_Aliases = 0; //:int
	
	var nStorages_index = 0; //:int
	
	var nFollowWebLinks = 0;//:int
	
	var nSplitDocuments = 0; //:int
	
	var nNonGlobals = 0; //:int
	
	var nTextEngines = 0; //:int
	
	var bOutOfPlaceGlobal = false; //:Boolean
	
	var bOutOfPlaceWebLinks = false;//:Boolean
	
	var globalsArray = [];//:string array
	
	var nGlobalsError = false; //:Boolean
	
	//Split info on each node given
	var nodeSplit = [];
	
	$.each(nodesByIdMap, function(index, value) {
		var splittedNode = nodesByIdMap[index].label.split("/")
		//console.log('splittedNode');console.log(splittedNode);
		
		nodeSplit.push({"catNo":splittedNode[0],"catNm":splittedNode[1],"label":splittedNode[2]});
		
		if(splittedNode[2].indexOf("Extractor")>-1){
			nExtractors++;
		}
		if(splittedNode[2].indexOf("Follow Web Links")>-1){
			nFollowWebLinks++;
		}
		
		if(splittedNode[2].indexOf("Store/Index Settings")>-1){
			nStorages_index++;
		}
	});
	
	
	if(nodes.length>0){ //nodes length 0 if no links
	
		//checking the first node
		//set isFirstExtractor to false if first element in nodes is not an extractor
		if(nodes[0].label.indexOf("Extractor")==-1){
			isFirstExtractor = false;
		}else if(nodes[0].label.indexOf("Extractor")>-1){
			isFirstExtractor = true;
		}else{
			isFirstExtractor = undefined;
		}
		
		for(var i=0; i<nodes.length; i++){
			//check if node is a global
			if(nodes[i].label.indexOf("globals")>-1){
				globalsArray.push(nodes[i].label);
				if(nodes[i-1].label.indexOf("Extractor")==-1){
					bOutOfPlaceGlobal = true;
				}
			}
			if(nodes[i].label.indexOf("Follow Web Links")>-1){
				globalsArray.push(nodes[i].label);
				if(nodes[i-1].label.indexOf("globals")==-1){
					bOutOfPlaceWebLinks = true;
				}
			}
		}
		
		if(globalsArray.length == $.unique(globalsArray).length){
			nGlobalsError = false;
		} else {
			nGlobalsError = true;
		}
	}
	
	
	for ( var i= 0; i < pipeline.length; i++ )
	{
		var sourceLine = pipeline[i];
		
		var sourceLineType = sourceLine.type;
		if(typeof(sourceLineType)!="undefined"){
		// TODO parse out folder from type
		var sfSplit = sourceLineType.split('/');
		
		var sourceLineFolder = sfSplit.length>1?sfSplit[1]:"";
		
		if ( i == 0 ){
			//firstFolder = sourceLineFolder;
		}
		if ( sourceLineFolder == "extractors" )
		{
			nExtractors++;
			nNonGlobals--;
			
			//if ( sourceLineType == "feed" )
		    if ( sourceLineType.toLowerCase().indexOf("feed") >-1 )
				nExtractor_Feed++;
			
			
			//if ( sourceLineType == templateDefs.WEB_EXTRACTOR_TYPE )
			if ( sourceLineType.toLowerCase().indexOf("web") >-1 )
				nExtractor_Web++;
		}
		
		if ( sourceLineFolder == "globals" )
		{
			if ( nNonGlobals > 0 ){
				bOutOfPlaceGlobal = true;
			}
			nGlobals++;
			
			if ( sourceLineType.toLowerCase().indexOf("harvest control settings")>-1 ){
				nGlobal_Harvest++;
				}
			
			if (  sourceLineType.toLowerCase().indexOf("add global javacript")>-1 ){
				nGlobal_Javascript++;
			}
			if ( sourceLineType.toLowerCase().indexOf("add lookup tables")>-1 ){
				nGlobal_LookupTables++;
			}
			
			//if ( sourceLineType == "aliases" ){
				//nGlobal_Aliases++;
				//}
		}
		else
		{
			nNonGlobals++;
		}
		
		if ( sourceLineType.toLowerCase().indexOf("search index settings")>-1 )
		{
			nStorages_index++;
		}
		
		if ( sourceLineType.toLowerCase().indexOf("follow web links")>-1)
		{
			if ( nNonGlobals > 1 ) // (ie this "links" counts as 1)
				bOutOfPlaceWebLinks = true;
			
			nFollowWebLinks++;
		}
		
		if ( sourceLineType.toLowerCase().indexOf("split documents")>-1 )
		{
			nSplitDocuments++;
		}
		
		if ( sourceLineType.toLowerCase().indexOf("automated text extraction") )
		{
			nTextEngines++;
		}
		
		} // if undefined
	} // for
	
	if ( bOutOfPlaceGlobal )
	{
		
		var e  =  new Error( 'Globals must be immediately after the extractor. Drag this element to an allowed position.'  );
		errors.push(e);
	}
	
	
	//if ( firstFolder != "input" && firstFolder != "" )
	if(isFirstExtractor === false)
	{
		var e  =  new Error( 'Only the extractors can be placed at the start!'  );
		errors.push(e);
	}
	
	if ( nExtractors >= 2 )
	{
		var e  =  new Error( 'Only one extractor in total is allowed!' );
		errors.push(e);
	}
	
	if ( nGlobal_Javascript >= 2 || nGlobal_LookupTables >= 2 || nGlobal_Aliases >= 2 || nGlobal_Harvest >= 2 || nGlobalsError == true )
	{
		var e  =  new Error( 'Only zero or one global of each type is allowed!' );
		errors.push(e);
	}

	// Allow web links in modified form for non-web/feed (keep changing my mind about this!)
//			if ( nFollowWebLinks > 0 && nExtractor_Feed == 0 && nExtractor_Web == 0 )
//			{
//				var e  =  new Error( '"Follow Web Links" can only be specified for Web/Feed extractors!' );
//				errors.push(e);				
//			}
	if ( nFollowWebLinks > 1 )
	{
		var e  =  new Error( '"Follow Web Links" can only be specified once!' );
		errors.push(e);				
	}			
	if ( bOutOfPlaceWebLinks ) 
	{
		var e  =  new Error( '"Follow Web Links" must be immediately after the extractors/globals!' );
		errors.push(e);				
	}
	
	if ( nStorages_index >= 2 )
	{
		var e  =  new Error( 'Only one "Store/Index Settings" is allowed' );
		errors.push(e);
	}
	
	return errors;
}

function isPrevElementInSamePath(flowElement, lastSourceElement){
	if ((flowElement.currentPath == null) || (flowElement.currentPath == "")) {
		   return  (lastSourceElement==null) || (lastSourceElement.criteria == null) || (lastSourceElement.criteria.indexOf("$PATH") < 0)   
		}
		else {
			if ((lastSourceElement==null) || (lastSourceElement.criteria == null)) {
				return false;
			}
			else {
				return lastSourceElement.criteria.indexOf(flowElement.currentPath) >= 0;
			}
		}
	return false;
}


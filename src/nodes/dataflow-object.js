( function(Dataflow) {
 
  // Dependencies
  var Base = Dataflow.prototype.node("base");
  var DataflowObject = Dataflow.prototype.node("dataflow-object");

  DataflowObject.Model = Base.Model.extend({
    defaults: function () {
      var defaults = Base.Model.prototype.defaults.call(this);
      defaults.type = "dataflow-object";
      defaults["output-type"] = "all";
      return defaults;
    },
    initialize: function (options){
      console.log(options);
      if (this.get("label")===""){
        this.set({label:"output"+this.id});
      }
      // super
      Base.Model.prototype.initialize.call(this, options);
    },
    inputdata: function (data) {
      // Forward data to parent graph
      this.get("parentNode").send(this.id, data);
    },
    toJSON: function(){
      var json = Base.Model.prototype.toJSON.call(this);
      json["output-type"] = this.get("output-type");
      return json;
    },
    inputs:[
      {
        id: "data",
        type: "all"
      },{
        id: "string",
        type: "string"
      }
    ],
    outputs:[
      // {
      //   id: "data",
      //   type: "all"
      // }
    ]
  });

  // DataflowObject.View = Base.View.extend({
  // });

}(Dataflow) );

( function(Dataflow) {
 
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var DataflowArray = Dataflow.prototype.node("dataflow-array");

  DataflowArray.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = "dataflow-array";
      //defaults.w = 200;
      //defaults.h = 400;
      return defaults;
    },
    inputinput: function(value){
      this.view.$inner.text(value);
    },
    inputs:[
      {
        id: "input",
        type: "all"
      },
      {
        id: "select",
        type: "string",
        options: "January February March April",
        value: "April"
      }
      // },
      // {
      //   id: "select2",
      //   type: "int",
      //   min: 0,
      //   max: 3,
      //   options: {sine:0, square:1, saw: 2, triangle: 3}
      // }
    ],
    outputs:[
      {
        id: "output",
        type: "all"
      }
    ]
  });

  DataflowArray.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
      this.$inner.text("view.$inner");
    }
  });

}(Dataflow) );

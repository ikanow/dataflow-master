//wrapped this script into a function so that it can be called from index.html
function ikanowParse(remoteJSON){
//Lazy Debug
/*
console.log('ikanowParse.js');
remoteJSON[3].fields[3] = {
	fieldName: "dropdown",
	type	 : "int",
	options	 : {var1:0,var2:1,var3:2}
};
console.dir(remoteJSON);
*/


for(var i=0; i<remoteJSON.length; i++){

 try{

( function(Dataflow) {
   var currComp = remoteJSON[i];
   
   //setting the outputs based on component type // 
   var ins = [];
   var outs = [];
   if(currComp.type=="input"){
	   outs = [
		  {
			id: "Output",
			type: "float"
		  }
		];
   }else if(currComp.type=="conditional"){
	    ins = [
	 		  {
	 				id: "Input",
	 				type: "float"
 			  }
           ];
		outs = [
		  {
			id: "True",
			type: "float"
		  },
		  {
			id: "False",
			type: "float"
		  }
		];
   }
   else {
	    ins = [
		 		  {
		 				id: "Input",
		 				type: "float"
	 			  }
	           ];
		   outs = [
		 		  {
		 			id: "Output",
		 			type: "float"
		 		  }
		 ];	   
   }
   
   //setting inputs based on component//
   var fieldCount = currComp.fields.length + ins.length;
   for(var j=0; j<currComp.fields.length; j++){
		var currField = {
			"id"	: currComp.fields[j].fieldname,
			"type"	: currComp.fields[j].type
		};
		//Dropdowns can be int or string. 
		//In order to turn an int or string into a dropdown, the "options" variable must be present. 
		//String dropdowns have the options variable in the form of options:"var1 var2 var3 varN"
		//int dropdowns have the form of options:{var1:0,var2:1,var3:2,var4:4, varN:N}
		
		if(currComp.fields[j].options!=undefined || typeof(currComp.fields[j].options)!=='undefined'){
			currField.options = currComp.fields[j].options;
		}
	
		if(currComp.fields[j].default!=undefined || typeof(currComp.fields[j].default)!=='undefined'){
			currField.default = currComp.fields[j].default;
		}
		ins.push(currField);
	}
  // Dependencies
  var BaseResizable = Dataflow.prototype.node("base-resizable");
  var nodeType = getSectionNum(currComp.type) + "/" + currComp.type + "/" + currComp.name;
  var component = Dataflow.prototype.node(nodeType);
  
  // attach sourceBuilder and sourceValidator
  if(typeof(currComp.sourceBuilder)!="undefined"){
	  if(typeof(Dataflow.sourceBuilderMap)==="undefined"){
		  Dataflow.sourceBuilderMap = {};
	  }
	  Dataflow.sourceBuilderMap[nodeType] = currComp.sourceBuilder; 
  }
  if(typeof(currComp.sourceValidator)!="undefined"){
	  if(typeof(Dataflow.sourceValidatorMap)==="undefined"){
		  Dataflow.sourceValidatorMap = {};
	  }
	  Dataflow.sourceValidatorMap[nodeType] = currComp.sourceValidator; 
  }
  
  component.Model = BaseResizable.Model.extend({
    defaults: function(){
      var defaults = BaseResizable.Model.prototype.defaults.call(this);
      defaults.type = nodeType;
	  defaults.label = nodeType;
      defaults.w = 205;     
      defaults.h = 38+(fieldCount*20);
      return defaults;
    },
    inputinput: function(value){
      //this.view.$inner.text(value);
    },
    inputs:ins,
    outputs: outs
  });

  component.View = BaseResizable.View.extend({
    initialize: function(options){
      BaseResizable.View.prototype.initialize.call(this, options);
    }
  });

   function getSectionNum(componentType){
	  var cToNum ={};
	  cToNum["conditional"]="0";
	  cToNum["input"]="1";
	  cToNum["globals"]="2";
	  cToNum["extractors"]="3";
	  cToNum["text"]="4";
	  cToNum["metadata"]="5";
	  cToNum["entities"]="6";
	  cToNum["storage"]="7";
	  if(typeof(cToNum[componentType])!="undefined"){
		  return cToNum[componentType];
	  }else{
		  return "";
	  }	  
  };
}(Dataflow) );


}catch(error){
//console.log('ikanowParse error');
//console.log(error);
}




}

}
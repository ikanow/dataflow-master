[
{
    "name":"pdf_reader",
    "type":"input",
    "description": "reads a set of PDF files from a local directory",
    "fields": [
    {
        "fieldname":"directory",
        "description":"Must point to a readable directory, start with file://, / terminated",
        "type": "string",
        "restrictions": "^file:.*/$",
        "default":"file:///path/to/directory/"
    },
    {
        "fieldname":"output_type",
        "description":"text, XML, or HTML",
        "type": "string",
        "restrictions": "^text|xml|html$", 
        "default": "text"
    },
    {
        "fieldname":"delete_when_complete",
        "description":"true to delete the file after ingestion",
        "type":"boolean",
        "default":false
    }
    ]
},
{
    "name":"contains_field",
    "type":"conditional",
    "description": "returns true if the specified field exists",
    "fields": [
    {
        "fieldname":"fieldname",
        "description":"The fieldname in dot notation to search for in the JSON",
        "type":"string"
    },
    {
        "fieldname":"isNested",
        "description": "if true then the fieldname can be anywhere in the path, if false then must be anchored to the start",
        "type":"boolean",
        "default": true
    }
    ]
}
]
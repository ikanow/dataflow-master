var initEnv = {};

var initEnvExample = {
		  "nodes": [
			{
			    "id": 7,
			    "label": "Regex to Metadata",
			    "type": "5. Metadata / Regex to Metadata",
			    "x": 1164.7166748046875,
			      "y": 53,
			    "state": {},
			    "w": 205,
			    "h": 150
			},
			{
			    "id": 6,
			    "label": "Regex to Entity",
			    "type": "6. Entities / Regex to Entity",
			    "x": 1177.7166748046875,
			    "y": 285,
			    "state": {},
			    "w": 205,
			    "h": 150
			},
		    {
			      "id": 5,
			      "label": "Salience Entity/Association Extractor",
			      "type": "1. Inputs / Salience Entity/Association Extractor",
			      "x": 915.7166748046875,
			      "y": 211,
			      "state": {},
			      "w": 205,
			      "h": 100
			    },
			    {
			        "id": 4,
			        "label": "NetOwl Entity/Assocation Extractor",
			        "type": "1. Inputs / NetOwl Entity/Assocation Extractor",
			        "x": 912.7166748046875,
			        "y": 86,
			        "state": {},
			        "w": 205,
			        "h": 100
			      },
			      {
			          "id": 3,
			          "label": "Split on Regex Match",
			          "type": "3. Splitting / Split on Regex Match",
			          "x": 688.7166748046875,
			          "y": 158,
			          "state": {},
			          "w": 200,
			          "h": 105
			        },
		    {
		      "id": 2,
		      "label": "Python Gazeteer Lookup",
		      "type": "Python Gazeteer Lookup",
		      "x": 417.7166748046875,
		      "y": 85,
		      "state": {},
		      "w": 205,
		      "h": 100
		    },
		    {
		        "id": 1,
		        "label": "JSON Windows Fileshare Extractor",
		        "type": "1. Inputs / JSON Windows Fileshare Extractor",
		        "x": 397.7166748046875,
		        "y": 254,
		        "state": {},
		        "w": 206,
		        "h": 265
		      }
		  ],
		  "edges": [
		    {
		      "source": {
		        "node": 5,
		        "port": "Output"
		      },
		      "target": {
		        "node": 6,
				//"node": 2,  //create cycle to 2
		        "port": "Input"
		      },
		      "route": 1
		    },
		    {
		      "source": {
		        "node": 4,
		        "port": "Output"
		      },
		      "target": {
		        "node": 7,
		        "port": "Input"
		      },
		      "route": 1
		    },
		    {
		        "source": {
		          "node": 7,
		          "port": "Output"
		        },
		        "target": {
		          "node": 6,
		          "port": "Input"
		        },
		        "route": 1
		    },
		    {
		      "source": {
		        "node": 3,
		        "port": "True"
		      },
		      "target": {
		        "node": 4,
		        "port": "Input"
		      },
		      "route": 11
		    },
		    {
		      "source": {
		        "node": 2,
		        "port": "Output"
		      },
		      "target": {
		        "node": 3,
		        "port": "Input"
		      },
		      "route": 1
		    },
		    {
		      "source": {
		        "node": 3,
		        "port": "False"
		      },
		      "target": {
		        "node": 5,
		        "port": "Input"
		      },
		      "route": 7
		    },
		    {
		      "source": {
		        "node": 1,
		        "port": "output"
		      },
		      "target": {
		        "node": 2,
		        "port": "Input"
		      },
		      "route": 5
		    }
		  ]
		};

var initEnvExample2 ={
	  "nodes": [
	    {
	      "id": 7,
	      "label": "Regex to Entity",
	      "type": "6. Entities / Regex to Entity",
	      "x": 1202,
	      "y": 399,
	      "state": {},
	      "w": 205,
	      "h": 150
	    },
	    {
	      "id": 1,
	      "label": "JSON Windows Fileshare Extractor",
	      "type": "1. Inputs / JSON Windows Fileshare Extractor",
	      "x": 397.7166748046875,
	      "y": 254,
	      "state": {},
	      "w": 206,
	      "h": 265
	    },
	    {
	      "id": 2,
	      "label": "Python Gazeteer Lookup",
	      "type": "Python Gazeteer Lookup",
	      "x": 417.7166748046875,
	      "y": 85,
	      "state": {},
	      "w": 205,
	      "h": 100
	    },
	    {
	      "id": 3,
	      "label": "Split on Regex Match",
	      "type": "3. Splitting / Split on Regex Match",
	      "x": 688.7166748046875,
	      "y": 158,
	      "state": {},
	      "w": 200,
	      "h": 105
	    },
	    {
	      "id": 8,
	      "label": "Generic Conditional",
	      "type": "3. Splitting / Generic Conditional",
	      "x": 751,
	      "y": 342,
	      "state": {},
	      "w": 205,
	      "h": 100
	    },
	    {
	      "id": 4,
	      "label": "NetOwl Entity/Assocation Extractor",
	      "type": "1. Inputs / NetOwl Entity/Assocation Extractor",
	      "x": 912.7166748046875,
	      "y": 86,
	      "state": {},
	      "w": 205,
	      "h": 100
	    },
	    {
	      "id": 5,
	      "label": "Salience Entity/Association Extractor",
	      "type": "1. Inputs / Salience Entity/Association Extractor",
	      "x": 915.7166748046875,
	      "y": 211,
	      "state": {},
	      "w": 205,
	      "h": 100
	    },
	    {
	      "id": 9,
	      "label": "Full text from metadata",
	      "type": "5. Metadata / Full text from metadata",
	      "x": 959,
	      "y": 452,
	      "state": {},
	      "w": 205,
	      "h": 100
	    },
	    {
	      "id": 6,
	      "label": "Regex to Entity",
	      "type": "6. Entities / Regex to Entity",
	      "x": 1159,
	      "y": 101,
	      "state": {},
	      "w": 205,
	      "h": 150
	    }
	  ],
	  "edges": [
	    {
	      "source": {
	        "node": 3,
	        "port": "True"
	      },
	      "target": {
	        "node": 4,
	        "port": "Input"
	      },
	      "route": 11
	    },
	    {
	      "source": {
	        "node": 2,
	        "port": "Output"
	      },
	      "target": {
	        "node": 3,
	        "port": "Input"
	      },
	      "route": 1
	    },
	    {
	      "source": {
	        "node": 3,
	        "port": "False"
	      },
	      "target": {
	        "node": 5,
	        "port": "Input"
	      },
	      "route": 7
	    },
	    {
	      "source": {
	        "node": 1,
	        "port": "output"
	      },
	      "target": {
	        "node": 2,
	        "port": "Input"
	      },
	      "route": 5
	    },
	    {
	      "source": {
	        "node": 8,
	        "port": "True"
	      },
	      "target": {
	        "node": 9,
	        "port": "Input"
	      },
	      "route": 1
	    },
	    {
	      "source": {
	        "node": 5,
	        "port": "Output"
	      },
	      "target": {
	        "node": 8,
	        "port": "Input"
	      },
	      "route": 1
	    },
	    {
	      "source": {
	        "node": 4,
	        "port": "Output"
	      },
	      "target": {
	        "node": 6,
	        "port": "Input"
	      },
	      "route": 1
	    },
	    {
	      "source": {
	        "node": 9,
	        "port": "Output"
	      },
	      "target": {
	        "node": 7,
	        "port": "Input"
	      },
	      "route": 1
	    },
	    {
	      "source": {
	        "node": 8,
	        "port": "False"
	      },
	      "target": {
	        "node": 6,
	        "port": "Input"
	      },
	      "route": 1
	    }
	  ]
	};